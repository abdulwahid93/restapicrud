package com.example.RestAPICRUD.api.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="nasabah")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Nasabah {
	@Id
	@GeneratedValue
	private int id;
	private String name;
	private String phone;
		
}
