package com.example.RestAPICRUD.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.RestAPICRUD.api.dao.NasabahDao;
import com.example.RestAPICRUD.api.model.Nasabah;

@RestController
@RequestMapping("/nasabah")
public class NasabahController {

	@Autowired
	private NasabahDao dao;
	
	@PostMapping("/create")
	public String createNasabah(@RequestBody Nasabah nasabah) {
		dao.save(nasabah);
		return "created nasabah success";
	}
	
	@GetMapping("/get")
	public List<Nasabah> getNasabah(){
		return (List<Nasabah>) dao.findAll();
	}
	
	@PutMapping("/update")
	public String updateNasabah(@RequestBody Nasabah nasabah) {
		dao.save(nasabah);
		return "update nasabah success";
	}
	
	@DeleteMapping("/delete/{id}")
	public String deleteNasabah(@PathVariable int id) {
		dao.deleteById(id);
		return "deleted nasabah success";
	}
}
